gnome-shell-extension-hard-disk-led (38-4) unstable; urgency=medium

  * Support gnome-shell version 48 (Closes: #1096049)
    - Update previous patch to include GNOME Shell 48 
    - Update gnome-shell dependency to include version 48
  * Update standards version to 4.7.1

 -- Jonathan Carter <jcc@debian.org>  Thu, 20 Feb 2025 16:32:57 +0200

gnome-shell-extension-hard-disk-led (38-3) unstable; urgency=medium

  * Add patch to mark compatible with GNOME Shell 47 (Closes: #1079251)
  * Bump maximum GNOME Shell to 47
  * Bump Standards Version to 4.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 12 Sep 2024 19:03:59 -0400

gnome-shell-extension-hard-disk-led (38-2) unstable; urgency=high

  * Team upload
  * Update minimum GNOME Shell dependency to match metadata.json
    (Closes: #1076264)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 15 Jul 2024 08:17:55 -0400

gnome-shell-extension-hard-disk-led (38-1) unstable; urgency=medium

  * Fix previously incomplete changelog entry
  * New upstream version (Closes: #1067438)
  * Remove debian/patches/support-gnome-46 (no longer needed)

 -- Jonathan Carter <jcc@debian.org>  Wed, 15 May 2024 12:47:47 +0200

gnome-shell-extension-hard-disk-led (37-1) unstable; urgency=medium

  * Update metadata.json to support gnome-shell version 46

 -- Jonathan Carter <jcc@debian.org>  Fri, 22 Mar 2024 13:51:51 +0200

gnome-shell-extension-hard-disk-led (37-1~exp1) experimental; urgency=medium

  * New upstream release 

 -- Jonathan Carter <jcc@debian.org>  Tue, 19 Dec 2023 19:56:13 +0200

gnome-shell-extension-hard-disk-led (35-1~exp1) experimental; urgency=medium

  * New upstream release (Closes: #1052102)
  * Update standards version to 4.6.2
  * Update gnome-shell dependency

 -- Jonathan Carter <jcc@debian.org>  Sat, 28 Oct 2023 20:30:36 +0200

gnome-shell-extension-hard-disk-led (34-1) unstable; urgency=medium

  * New upstream release 
  * Update gnome-shell dependency version to (<< 45~)

 -- Jonathan Carter <jcc@debian.org>  Mon, 19 Jun 2023 10:58:02 +0200

gnome-shell-extension-hard-disk-led (33-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Sat, 19 Nov 2022 11:35:51 +0200

gnome-shell-extension-hard-disk-led (32-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.6.1
  * Remove debian patch for gnome-shell 43 support (no longer needed)

 -- Jonathan Carter <jcc@debian.org>  Mon, 31 Oct 2022 12:19:31 +0200

gnome-shell-extension-hard-disk-led (30-4) unstable; urgency=medium

  * Team upload
  * Fix maximum gnome-shell version

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sat, 10 Sep 2022 22:28:59 -0400

gnome-shell-extension-hard-disk-led (30-3) unstable; urgency=medium

  * Team upload
  * Add patch to mark compatible with GNOME Shell 43
  * debian/control: Bump maximum gnome-shell to 43 (Closes: #1018188)

 -- Jesús Soto <jesus.soto@canonical.com>  Thu, 08 Sep 2022 16:24:54 -0400

gnome-shell-extension-hard-disk-led (30-2) unstable; urgency=medium

  [ Simon McVittie ]
  * d/p/extension-Avoid-deprecated-conversion-from-Uint8Array-to-.patch:
    Add patch to improve forward-compatibility with future gjs and
    silence a warning
  * d/p/metadata-Declare-compatibility-with-GNOME-Shell-42.patch:
    Add patch to mark as compatible with GNOME Shell 42 (Closes: #1008535)
  * Depend on a Shell version for which metadata.json declares compatibility

  [ Jonathan Carter ]
  * Remove patches introduced by MR, but are no longer needed

 -- Jonathan Carter <jcc@debian.org>  Wed, 10 Aug 2022 10:16:44 +0200

gnome-shell-extension-hard-disk-led (30-1) unstable; urgency=medium

  * New upstream release (Closes: #1008535)
  * Fix incorrect standards version (4.6.1 > 4.6.0)

 -- Jonathan Carter <jcc@debian.org>  Mon, 11 Apr 2022 09:42:19 +0200

gnome-shell-extension-hard-disk-led (29-1) unstable; urgency=medium

  * New upstream release
  * Remove convenience.js from debian/install

 -- Jonathan Carter <jcc@debian.org>  Tue, 01 Mar 2022 09:19:49 +0200

gnome-shell-extension-hard-disk-led (25-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update pattern for GitHub archive URLs from /<org>/<repo>/tags
    page/<org>/<repo>/archive/<tag> -> /<org>/<repo>/archive/refs/tags/<tag>.

  [ Jonathan Carter ]
  * New upstream release (Closes: #993181)
  * Update standards version to 4.6.1
  * Drop versioned depends on gnome-shell
  * Update copyright years

 -- Jonathan Carter <jcc@debian.org>  Thu, 02 Sep 2021 17:37:19 +0200

gnome-shell-extension-hard-disk-led (24-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan Carter <jcc@debian.org>  Thu, 10 Dec 2020 20:57:02 +0200

gnome-shell-extension-hard-disk-led (23-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.5.1

 -- Jonathan Carter <jcc@debian.org>  Wed, 09 Dec 2020 10:20:45 +0200

gnome-shell-extension-hard-disk-led (22-1) unstable; urgency=medium

  * New upstream release (Closes: #971041)
  * Update watch file for change in upstream archive names
  * Update to debhelper compat (= 13)
  * Declare Rules-Requires-Root: no

 -- Jonathan Carter <jcc@debian.org>  Wed, 21 Oct 2020 10:08:04 +0200

gnome-shell-extension-hard-disk-led (19-2) unstable; urgency=medium

  * Move Vcs to gnome-team on salsa.debian.org
  * Upgrade to debhelper-compat (=12)
  * Update standards version to 4.5.0
  * Update copyright years

 -- Jonathan Carter <jcc@debian.org>  Tue, 25 Feb 2020 11:19:42 +0200

gnome-shell-extension-hard-disk-led (19-1) unstable; urgency=medium

  * New upstream release
  * Update maintainer email address
  * Update compat to level 11
  * Update standards version to 4.1.3
  * Update copyright years
  * Migrate VCS to salsa.debian.org
  * Add watch file

 -- Jonathan Carter <jcc@debian.org>  Tue, 06 Mar 2018 11:52:38 +0200

gnome-shell-extension-hard-disk-led (13~git20160916.6fe27b2-1) unstable; urgency=medium

  * Initial release (Closes: #842701)

 -- Jonathan Carter <jcarter@linux.com>  Mon, 31 Oct 2016 14:00:44 +0200
